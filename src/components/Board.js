import { Component } from 'react';
import Square from './Square'
import '../styles/Board.css';

class Board extends Component {
    render() {
        return <div className="board">
            <Square />
            <Square />
            <Square />
        </div>
    }
}

export default Board;